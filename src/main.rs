use std::env;
use std::collections::HashMap;
use simple_error::SimpleError;
use serde::{Deserialize, Serialize, Serializer, Deserializer};
use chrono::{DateTime, Duration, Local};
use reqwest::{Client};
//use url::{Url};
use opentelemetry::api::Provider;
use opentelemetry::sdk;
use opentelemetry::global;
use tracing::{span, error};
use tracing_attributes::instrument;
use tracing_subscriber::EnvFilter;
use tracing_subscriber::prelude::*;
use tracing_futures::Instrument;
use bb8::Pool;
use bb8_postgres::PostgresConnectionManager;

#[derive(Debug, Serialize, Deserialize)]
struct WorkflowVCSInfo {
    branch: Option<String>,
    revision: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct WorkflowRun {
    id: String,
    status: String,
    name: String,
    #[serde(deserialize_with = "option_datefmt")]
    #[serde(serialize_with = "option_spreadsheet_serialize")]
    stopped_at: Option<DateTime<Local>>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Workflows {
    items: Vec<WorkflowRun>,
    next_page_token: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct PipelineRun {
    id: String,
    number: i64,
//    state: String,
    #[serde(serialize_with = "spreadsheet_serialize")]
    #[serde(default = "missing_date")]
    created_at: DateTime<Local>,
    #[serde(serialize_with = "spreadsheet_serialize")]
    #[serde(default = "missing_date")]
    updated_at: DateTime<Local>,
    #[serde(skip_serializing)]  // flatten doesn't work, so hack our own flattening
    vcs: WorkflowVCSInfo,
    #[serde(skip_deserializing)]
    branch: Option<String>,
    #[serde(skip_deserializing)]
    is_master: bool, // convenience for spreadsheet analysis
    #[serde(skip_deserializing)]
    status: String,
    #[serde(skip_deserializing)]
    workflow_id: String,
    #[serde(skip_deserializing)]
    name: String,
    #[serde(skip_deserializing)]
    #[serde(serialize_with = "option_spreadsheet_serialize")]
    stopped_at: Option<DateTime<Local>>,
    #[serde(skip_deserializing)]
    duration: f64,
}

#[derive(Debug, Serialize, Deserialize)]
struct Pipelines {
    items: Vec<PipelineRun>,
    next_page_token: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct JobRun {
    #[serde(default)]
    job_number: i64,
    status: String,
    name: String,
    started_at: Option<DateTime<Local>>,
    stopped_at: Option<DateTime<Local>>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Jobs {
    items: Vec<JobRun>,
    next_page_token: Option<String>
}

#[derive(Debug, Serialize, Deserialize)]
struct Action {
    #[serde(default)]
    output_url: String,
    status: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Steps {
    actions: Vec<Action>,
    name: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct JobDetail {
    branch: String,
    build_num: u32,
//    body: Option<String>,
//    lifecycle: String,
//   outcome: String,
    #[serde(serialize_with = "spreadsheet_serialize")]
    #[serde(default = "missing_date")]
    start_time: DateTime<Local>,
    status: String,
    #[serde(skip_serializing)]
    steps: Vec<Steps>,
    #[serde(skip_deserializing)]
    name: String,
    #[serde(skip_deserializing)]
    workflow_name: String,
    build_time_millis: i64,
    #[serde(skip_deserializing)]
    #[serde(serialize_with = "spreadsheet_serialize")]
    #[serde(default = "missing_date")]
    stop_time: DateTime<Local>,
    #[serde(skip_deserializing)]
    output_url: String,
    #[serde(skip_deserializing)]
    errmsg: String,
}

fn datefmt<'de, D>(deserializer: D) -> Result<DateTime<Local>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    match DateTime::parse_from_rfc3339(&s)
    {
        Ok(dt) => {let converted: DateTime<Local> = DateTime::from(dt); Ok(converted)},
        Err(_) => Err(serde::de::Error::custom("datefmt")),
    }
    
}

fn option_datefmt<'de, D>(deserializer: D) -> Result<Option<DateTime<Local>>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    struct Wrapper(#[serde(deserialize_with = "datefmt")] DateTime<Local>);

    let v = Option::deserialize(deserializer)?;
    Ok(v.map(|Wrapper(a)| a))
}

fn missing_date() -> DateTime<Local> {
    Local::now()
}

fn spreadsheet_serialize<S> (date: &DateTime<Local>,serializer: S,) -> Result<S::Ok, S::Error>
where S: Serializer,
{
    let s = format!("{}", date.format("%Y-%m-%d %H:%M:%S"));
    serializer.serialize_str(&s)
}

fn option_spreadsheet_serialize<S> (date: &Option<DateTime<Local>>,serializer: S,) -> Result<S::Ok, S::Error>
where S: Serializer,
{
    match date {
        Some(d) => {
            let s = format!("{}", d.format("%Y-%m-%d %H:%M:%S"));
            return serializer.serialize_str(&s)
        },
        None => return serializer.serialize_str("")
    }

 //   #[derive(Serialize)]
 //   struct Wrapper(#[serde(serialize_with = "spreadsheet_serialize")] Result);
 //   let v = Option::serialize(date,serializer);
 //   Ok(v.map(|Wrapper(a)| a))
}

#[instrument(skip(api_key))]
async fn get_job_detail(c: &Client, api_key: &str, job_id: u32) -> Result<JobDetail, reqwest::Error> {
    let url = &(String::from("https://circleci.com/api/v1.1/project/github/procore/procore/") + &job_id.to_string());
    let resp = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await?
        .json::<JobDetail>()
        .await {
            Ok(resp) => resp,
            Err(e) => {error!(error = %e); println!("JobDetail err: {:#?}", e); return Err(e);},
        };
    Ok(resp)
}

//hack to deal with Rust being confused by unicode character boundaries when using slices or built-in truncate. Return last [x] chars of string
/*
fn truncate(s: &str, max_chars: usize) -> &str {
    if s.len() <= max_chars {return s;}
    match s.char_indices().nth(s.len() - max_chars) {
        None => if s.len() <= max_chars {s} else {""},
        Some((idx, _)) => &s[idx..],
    }
}*/

#[derive(Debug, Serialize, Deserialize)]
struct Artifact {
    node_index: u32,
    url: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Artifacts {
    items: Vec<Artifact>,
    next_page_token: Option<String>,
}

#[instrument(skip(api_key))]
async fn get_artifact(url: &str, api_key: String, c: &Client, times: &mut HashMap<String, f64>) 
    -> Result<(), SimpleError> 
{
    let f = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await {
            Ok(f) => {f},
            Err(e) => {println!("Couldn't retrieve test output {:#?} at URL {:#?}", e, url); simple_error::bail!("reason: {}", e);},
    };

    *times = match f.json::<HashMap<String, f64>>().await {
        Ok(s) => {s},
        Err(e) => {simple_error::bail!("reason: {}", e);},
    };

    Ok(())
}

#[instrument(skip(api_key))]
async fn get_all_artifacts(workflow: &WorkflowRun, job: &JobRun, api_key: &str, c: &Client, db: &bb8::PooledConnection<'_, bb8_postgres::PostgresConnectionManager<tokio_postgres::NoTls>>)
    -> Result<(), Box<dyn std::error::Error>> 
{
    let url = &(String::from("https://circleci.com/api/v2/project/github/procore/procore/") + &job.job_number.to_string() + "/artifacts");
    let resp = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await?
        .json::<Artifacts>()
        .await {
            Ok(resp) => resp,
            Err(e) => {error!("Error retrieving artifacts for job {} err: {:#?}", job.job_number, e); return Err(e.into());},
        };


    for i in resp.items {
        let mut times: HashMap<String, f64> = HashMap::new();
        match get_artifact(i.url.as_str(), String::from(api_key), c, &mut times).await {
            Ok(_) => {
                for f in times {
                    if f.1 > 10.0 { // be kind to the database and discard all test files that didn't take at least 10 sec
                        if let Err(e) = db.execute(
                            "INSERT INTO Test_times (job_number, started_at, status, file_name, runtime_sec) 
                            VALUES ($1, $2, $3, $4, $5)",
                            &[&job.job_number, &job.started_at, &workflow.status, &f.0, &f.1],
                        ).instrument(tracing::info_span!("write times to DB")).await {
                            eprintln!("DB insert failed {:?}",e);
                            error!("DB insert failed {:?}",e);
                            return Err(e.into());
                        };
                    }
                }
            },
            Err(e) => {println!("get_artifact failed with error {:#?}",e);},
        }
    }
    Ok(())
}

#[instrument(skip(api_key))]
async fn get_jobs(c: &Client, api_key: &str, workflow: &WorkflowRun, db: &bb8::PooledConnection<'_, bb8_postgres::PostgresConnectionManager<tokio_postgres::NoTls>>) 
    -> Result<(), SimpleError>
{
    let url = &(String::from("https://circleci.com/api/v2/workflow/") + &workflow.id + "/job");
    let r = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await {
            Ok(w) => {w},
            Err(e) => {simple_error::bail!("reason: {}", e);},
    };
    let resp = match r.json::<Jobs>().await {
        Ok(resp) => resp,
        Err(e) => {error!(error = %e); println!("get_jobs fatal error {:#?} on URL {:#?}", e, url); panic!();},
    };

    for i in &resp.items {
        if i.name == "push_knapsack_report" && i.status == "success" { // get test run times
            if let Err(e) = get_all_artifacts(workflow, &i, api_key, &c, db).await {
                simple_error::bail!("reason: {}", e);
            }
        }
/*        if i.status == "failed" && i.name != "rails-next-failing-tests" && i.name != "strong-parameters" { //ignore jobs intended to fail
            let mut jd = get_job_detail(c, api_key, i.job_number).await?;
            for j in &jd.steps {
                for k in &j.actions {
                    if k.status == "failed" {
                        match Url::parse(&k.output_url) {
                            Ok(u) => {
                                jd.output_url = u.to_string();
                                jd.errmsg = match c.get(u)
                                    .send()
                                    .await {
                                        Ok(em) => {
                                            let errmsg = em.text().await?;
                                            println!("Job {} failed at {:#?} step '{}'",i.name, i.started_at.unwrap_or_else(|| Local::now()), j.name); 
                                            truncate(&errmsg,2048).to_string() // Only output first 2k chars of output
                                        },
                                        Err(e) => {error!(error = %e); println!("Couldn't retrieve error output {:#?} on URL {:#?}", e, url); "N/A".to_string()},
                                    };
                                

                            }
                            Err(e) => {error!(error = %e); println!("Couldn't retrieve error output {:#?} on URL {:#?}", e, url);},
                        }
                    } 
                }
                if j.name.contains("Container circleci/postgres") { // look for database in recovery mode errors
                }
            }
            jd.name = i.name.clone();
            jd.workflow_name = workflow.name.clone();
        }
*/

    }    
    Ok(())
}

async fn get_pipeline(c: &Client, api_key: &str, i: &mut PipelineRun, pool: &Pool<PostgresConnectionManager<tokio_postgres::NoTls>>) 
    -> Result<(), SimpleError>
{
    let url = &(String::from("https://circleci.com/api/v2/pipeline/") + &i.id + "/workflow");
    let w = match c.get(url)
    .query(&[("circle-token",api_key)])
    .send()
    .instrument(tracing::info_span!("get workflow"))
    .await {
        Ok(w) => {w},
        Err(e) => {simple_error::bail!("reason: {}", e);},
    };

    let wfs = match w.json::<Workflows>().await {
        Ok(resp) => {resp},
        Err(e) => {error!(error = %e); println!("get workflow fatal error {:#?} on URL {:#?}", e, url); simple_error::bail!("reason: {}", e);},
    };

    if wfs.items.len() > 0 { //ignore pipelines with no workflow results
        let wf = &wfs.items.last().unwrap(); // CircleCI returns multiple workflows in most-recent-first order, so grab the first workflow if there are multiple. This will ignore re-runs
        i.branch = i.vcs.branch.clone(); //manually flatten struct for CSV serialization TODO: refactor now that we're not using CSVs
        i.name = wf.name.clone();
        i.workflow_id = wf.id.clone();
        i.status = wf.status.clone();
        i.stopped_at = wf.stopped_at.clone();
        i.duration = i.stopped_at.unwrap_or_else(|| i.created_at).signed_duration_since(i.created_at).num_seconds() as f64 / 60.0;
        i.is_master = i.branch.clone().unwrap_or_default() == "master";

        if i.status != "running" && i.status != "failing" { //don't write workflows still in progress
            let db = match pool.get().instrument(tracing::info_span!("get DB connection")).await {
                Ok(p) => {p},
                Err(e) => {
                    error!("DB insert failed {:?}",e);
                    simple_error::bail!("reason: {}", e);
                },
            };
            if let Err(e) = db.execute(
                "INSERT INTO pipelines (id, number, created_at, updated_at, branch, is_master, status, workflow_id, name, duration) 
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
                &[&i.id, &i.number, &i.created_at, &i.updated_at, &i.branch, &i.is_master, &i.status, &i.workflow_id, &i.name, &i.duration],
            ).instrument(tracing::info_span!("write DB")).await {
                error!("DB insert failed {:?}",e);
                simple_error::bail!("reason: {}", e);
            };
            if let Err(e) = get_jobs(&c, api_key, &wf, &db).await {
                error!("get_jobs failed {:?}",e);
            }    
        } 
   }
   Ok(())
}

fn init_tracer() -> Result<(), Box<dyn std::error::Error>> {
    let exporter = opentelemetry_jaeger::Exporter::builder()
        .with_agent_endpoint("127.0.0.1:6831".parse().unwrap())
        .with_process(opentelemetry_jaeger::Process {
            service_name: "circle_stats".to_string(),
            tags: Vec::new(),
        })
        .init()?;
    let provider = sdk::Provider::builder()
        .with_simple_exporter(exporter)
        .with_config(sdk::Config {
            default_sampler: Box::new(sdk::Sampler::AlwaysOn),
            ..Default::default()
        })
        .build();
    let tracer = provider.get_tracer("tracing");

    let filter_layer = EnvFilter::try_from_default_env() // get filter config from RUST_LOG envar; if not present default to INFO level
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    tracing_subscriber::registry()
        .with(filter_layer)
        .with(opentelemetry)
        .try_init()?;

    global::set_provider(provider);

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    init_tracer()?;

    let root = span!(tracing::Level::INFO, "app_start");
    let _enter = root.enter();
    let client = reqwest::Client::new();
    let oldest_date = Local::now() - Duration::days(31);

    let api_key = env::var("CIRCLE_API_KEY").unwrap_or_else(|e| {
        panic!("could not find CIRCLE_API_KEY {}", e)
    });
    if api_key.is_empty() {
        panic!("could not find CIRCLE_API_KEY");
    } else {
        println!("Found CIRCLE_API_KEY")
    }

    let pg_host = env::var("PG_HOST").unwrap_or_else(|e| {
        panic!("could not find PG_HOST {}", e)
    });
    if pg_host.is_empty() {
        panic!("could not find PG_HOST");
    } else {
        println!("Found PG_HOST")
    }

    let mut page_token: String = "".to_string();

    let pg_mgr = PostgresConnectionManager::new_from_stringlike(pg_host,tokio_postgres::NoTls,).unwrap();

    let pool = match Pool::builder().build(pg_mgr).await {
        Ok(pool) => pool,
        Err(e) => panic!("bb8 error {}", e),
    };

    let mut done = false;

    while !done {
        let mut params: Vec<(&str, &str)> = vec!(("circle-token",api_key.as_str()));
        if !page_token.is_empty() {params.push(("page-token",page_token.as_str()))}
        let resp = match client.get("https://circleci.com/api/v2/project/github/procore/procore/pipeline")
            .query(&params)
            .send()
            .await?
            .json::<Pipelines>()
            .instrument(tracing::info_span!("get pipelines"))
            .await{
                Ok(resp) => resp,
                Err(e) => {error!(error = %e); println!("get pipelines fatal error {:#?}", e); panic!();},
            };

        if resp.items[0].created_at < oldest_date || resp.next_page_token.is_empty() {break}
        page_token = resp.next_page_token;
        println!("{}", resp.items[0].created_at);
        let mut tasks = vec![];

        for mut i in resp.items {
            let apic = api_key.clone();
            let c2 = client.clone();
            let pool = pool.clone();
            tasks.push(tokio::spawn(async move {
                match get_pipeline(&c2, &apic, &mut i, &pool).instrument(tracing::info_span!("get_pipeline")).await {
                    Ok(_) => {true},
                    Err(e) => {eprintln!("Error from get_pipeline {:?}",e); false},
                }
            }));

        }
        for task in tasks {
            match task.instrument(tracing::info_span!("get_pipeline gather")).await {
                Err(e) => {
                    eprintln!("Task errored out, finishing up {:?}",e);
                    done = true;
                },
                Ok(k) => {if !k {done = true;}}
            };
        }
    }
    drop(_enter); // ideally unnecessary; seeing if this corrects parent span flush issue with Jaeger exporter
    Ok(())
}