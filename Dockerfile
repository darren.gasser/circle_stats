FROM alpine:latest

RUN addgroup -g 1000 circle_stats

RUN adduser -D -s /bin/sh -u 1000 -G circle_stats circle_stats

WORKDIR /home/circle_stats/bin/

COPY target/x86_64-unknown-linux-musl/release/circle_stats .

RUN chown circle_stats:circle_stats circle_stats

USER circle_stats

ENV CIRCLE_API_KEY \
    RUST_LOG \
    PG_HOST

CMD ["./circle_stats"]